Rails.application.routes.draw do
  resources :publishing_houses
  resources :authors
  resources :books
  mount_ember_app :bookstore, to: "/"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
